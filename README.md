# bdd fermel

Gestion d'une exploitation agricole. 

Développement de la base de données qui sera le coeur de la gestion agricole, et un module de SCeGeBa.
Cette basse de données capitalisera les données :
- MTO
- pedologiques
- agricoles
    -   par unité de culture, i.e. par planche ;
    -   fertilisations ;
    -   travail du sol ;
    -   gestion des adventices ;
    -   traitements des maladies et parasites.
- Sockage des messages d'alerte.

Pour le moment elle est conçue pour répondre aux besoins de Nature & progrès, afin de faciliter les visites et le suivi des mentions pour les expoitants.

Donc elle inclue les données du dossier de demande de mention, et aussi ce qui peut être utile à l'exploitant dans sa vie de tous les jours.
